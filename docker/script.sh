#!/bin/bash
sleep 100 
if [ -z "$1" ]			
then
    $1=$URL
    chemin=$1
else
 chemin="$1"
 
count_1=$(curl -s -m 5 $1/count | jq '.pingCount' 2>/dev/null)
 pong=$(curl -s -m 5 $1/ping | jq '.message' 2>/dev/null)	
    if [  $pong=="pong" ]
        then
        echo "Message correct"
    else
        echo "Message incorrect"
        exit 1
    fi

 count_2=$(curl -s -m 5 $1/count | jq '.pingCount' 2>/dev/null)
 score=$(($count_2 - $count_1))
    if [ $(echo $score | bc) -eq 1 ]
    then
        echo "Compteur correcte"
    else
        echo "Compteur incorrecte"
        exit 1
    fi
echo "Tests unitaires termines, l'application fonctionne"
exit 0
fi
