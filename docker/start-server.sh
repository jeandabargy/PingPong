#!bin/bash
/usr/local/glassfish4/bin/asadmin start-domain
/usr/local/glassfish4/bin/asadmin deploy --port 4848 --host localhost /usr/local/glassfish4/PingCount.war
/usr/local/glassfish4/bin/asadmin stop-domain
/usr/local/glassfish4/bin/asadmin start-domain --verbose
