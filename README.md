![ALT](devops.png)

# Projet Ping Pong - Groupe 9
###### Par Maxime CALVES et Jean-Daniel Bargy

[![pipeline status](https://gitlab.com/ilaipaconlui/PingPong/badges/master/pipeline.svg)](https://gitlab.com/ilaipaconlui/PingPong/commits/master)

## Faire attention 
Voici unes listes de contraintes liée à la technologie Java et le serveur d'hébergement :  
1. L'url pour accéder aux pages ping et count est "http://IP_SERVEUR:8080/PingCount/rsc/pingcount/ping" et "http://IP_SERVEUR:8080/PingCount/rsc/pingcount/count" 
2. Mettre un délais avant le lancement du conteneur de test d'environ 70-100 secondes

## Description
Bienvenue sur le mini-projet orienté DEVOPS avec intégration continue.
L'objectif de ce projet est d'obtenir deux pages web contenant une première page 
Ping et une deuxième page Count. La deuxième page contient un score qui s'incrémente
au fur et à mesure de générer des requêtes pour la page Ping.

## Technologies
Ce projet se compose d'un serveur Glassfish en version 4.1 et d'un code source java en version 8.

## Principes de fonctionnement du Dockerfile applicatif
Création d'une image docker contenant le serveur Glassfish en version 4.1 et java en version 8.
Puis on déploiement le code source java contenant le projet PingPong.

## Lancer le projet PingPong
### Connexion au dépôt du projet
`> docker login registry.gitlab.com`

### Téléchargement des images en local
`> docker pull registry.gitlab.com/ilaipaconlui/pingpong/pingpong:master`  

`> docker pull registry.gitlab.com/ilaipaconlui/pingpong/test:latest`

### Lancement des conteneurs de manière individuel
`> docker run -d -p 80:8080 pingcount`  
`> docker run -d test`

### Lancement des conteneurs avec docker-compose
`> docker-compose build`  
`> docker-compose up -d`

## CI
L'intégration continue permet au projet d'être automatiser avec les étapes de 
construction des sources, de construction de images docker et des tests unitaires du conteneur applicatif.


Faites remonter les problèmes via les issues.
