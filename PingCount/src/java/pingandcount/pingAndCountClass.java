/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pingandcount;

import java.io.StringReader;
import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jd
 */
@XmlRootElement(name = "pingcount")
public class pingAndCountClass {
    private static int count;
    private static String pong; 

    static{
        count = 0;
        pong = "{\"message\" : \"pong\"}";
    }
    
    public static String getPong(){
        return pong;
    }
    
    public static void incrementCount(){
        count++;
    }
    
    public static String getCount(){
        return "{\"pingCount\" : "+count+"}"; 
    }
    
}
