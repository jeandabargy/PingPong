/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pingandcount;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author jd
 */
@Path("pingcount")
@Produces("application/json")
public class pingAndCountResource {
    
    @GET
    @Path("/ping")
    public Response getPing() {
        System.out.println("ping");
        pingAndCountClass.incrementCount();
        return Response
          .status(Status.OK)
          .entity(pingAndCountClass.getPong())
          .build();
    }
    
    @GET
    @Path("/count")
    public Response getCount() {
        System.out.println("count");
        return Response
          .status(Status.OK)
          .entity(pingAndCountClass.getCount())
          .build();
    }

}
